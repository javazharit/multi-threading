package me.babayan;

public abstract class Profile extends Thread {

	protected ProfileType profileType;
	protected int actionDuration;

	/**
	 * Construction.
	 *
	 * @param profileType
	 * @param actionDuration
	 */
	public Profile(ProfileType profileType, int actionDuration) {
		this.profileType = profileType;
		this.actionDuration = actionDuration;
	}

	public ProfileType getProfileType() {
		return profileType;
	}

	public void setProfileType(ProfileType profileType) {
		this.profileType = profileType;
	}

	public int getActionDuration() {
		return actionDuration;
	}

	public void setActionDuration(int actionDuration) {
		this.actionDuration = actionDuration;
	}
}
