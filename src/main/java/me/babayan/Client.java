package me.babayan;

import org.apache.log4j.*;

import java.util.List;

public class Client extends Profile {

	/** Shared object queue */
    private final List<String> taskQueue;

	// Log4j logger
	private static Logger __log = Logger.getLogger(Client.class);

	/**
	 * Construction.
	 *
	 * @param profileType
	 * @param sharedQueue
	 */
    public Client(ProfileType profileType, int actionDuration, List<String> sharedQueue) {
		super(profileType, actionDuration);
        this.taskQueue = sharedQueue;
    }

	/**
	 * Runs on thread start.
	 */
    @SuppressWarnings("InfiniteLoopStatement")
	@Override
    public void run() {
        while (true) {
            try {
                eating();
            } catch (InterruptedException ex) {
                __log.error(ex);
            }
        }
    }

    private void eating() throws InterruptedException {
        synchronized (taskQueue) {
            while (taskQueue.isEmpty()) {
                taskQueue.wait();
            }

			// expeting action duration to pass
            Thread.sleep(actionDuration * 1000);

			// eating/consuming the well expected MEAT
            String str = taskQueue.remove(0);
            __log.info(String.format("%s ate %s and waiting for the next piece of meat!", profileType.getProfile(), str));

			// notifying interested parties about my hapiness
            taskQueue.notifyAll();
        }
    }
}