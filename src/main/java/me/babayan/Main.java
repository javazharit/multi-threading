package me.babayan;

import java.util.ArrayList;
import java.util.List;

public class Main {
	private static final int QUEUE_SIZE = 1;

    public static void main(String[] args) {
    	// task queue
        List<String> taskQueue = new ArrayList<>();

        // setting timings for cook's/clients' operations
		String[] timings = new String[2];
		if (args.length == 0) {
			timings[0] = String.valueOf(2);
			timings[1] = String.valueOf(3);
		} else if (args.length == 1) {
			timings[0] = args[0];
			timings[1] = String.valueOf(3);
		} else {
			timings[0] = args[0];
			timings[1] = args[1];
		}

		// acquiring cooking time setting from arguments
		String cookingTime = timings[0];
		int cookingSec = Integer.parseInt(cookingTime);

		// acquiring eating time setting from arguments
		String eatingTime = timings[1];
		int eatingSec = Integer.parseInt(eatingTime);

		// creating and starting Cook thread
        Cook cook = new Cook(cookingSec, QUEUE_SIZE, taskQueue);
		cook.start();

		// creating and starting a client thread from Armenia
        Client clientArmenian = new Client(ProfileType.CLIENT_AM, eatingSec, taskQueue);
		clientArmenian.start();

		// creating and starting a client thread from Georgia
		Client clientGeorgian = new Client(ProfileType.CLIENT_GE, eatingSec, taskQueue);
		clientGeorgian.start();
    }

}
