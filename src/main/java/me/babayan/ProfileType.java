package me.babayan;

public enum ProfileType {

	CLIENT_AM (1, "CLient [AM]"),
	CLIENT_GE (2, "CLient [GE]"),
	COOK 	  (3, "Cook");

	ProfileType(final int value, final String profile) {
		this.value = value;
		this.profile = profile;
	}

	public int getValue() {
		return value;
	}

	public String getProfile() {
		return profile;
	}

	private int value;
	private String profile;
}