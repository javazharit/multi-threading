package me.babayan;

import org.apache.log4j.*;

import java.util.List;
import java.util.Random;

public class Cook extends Profile {

	/** Shared object queue limit */
    private final int queueSize;

	/** Shared object queue */
    private final List<String> sharedQueue;

	// Log4j logger
	private static Logger __log = Logger.getLogger(Cook.class);

	/**
	 * Construction.
	 *
	 * @param sharedQueue
	 * @param queueSize
	 */
    public Cook(int actionDuration, int queueSize, List<String> sharedQueue) {
		super(ProfileType.COOK, actionDuration);

        this.queueSize = queueSize;
        this.sharedQueue = sharedQueue;
    }

	/**
	 * Runs on thread start.
	 */
	@SuppressWarnings("InfiniteLoopStatement")
    @Override
    public void run() {
        while (true) {
            try {
				String barbecue = randomMeatBarbecue();
                cook(barbecue);
            } catch (InterruptedException ex) {
				__log.error(ex);
            }
        }
    }

    private void cook(String barbecue) throws InterruptedException {
        synchronized (sharedQueue) {
			while (sharedQueue.size() == queueSize) {
				__log.info(String.format("%s: Barbecue cooked, I am waiting for the guys to eat, to cook a new batch!", profileType.getProfile()));
                sharedQueue.wait();
            }

			// expeting action duration to pass
            Thread.sleep(actionDuration * 1000);

			// preparing/producing the well expected MEAT
            sharedQueue.add(barbecue);
            __log.info(String.format("Cooked: %s", barbecue));

			// notifying the hungry clients
            sharedQueue.notifyAll();
        }
    }

    /**
     * Returns a random barbecue name from pre-defined values.
     */
    private String randomMeatBarbecue() {
        Random randomGenerator = new Random();
        int type = randomGenerator.nextInt(3);

        if (type == 0) {
            return "PORK";
        } else if (type == 1) {
			return "BEEF";
        } else {
			return "LAMB";
        }
    }
}

